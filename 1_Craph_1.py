# Реализуем возможность добавлять/удалять/проверять наличие ребер

class Graph:
    def __init__(self, v):
        # граф задаем с помощью списков смежности
        self._v = v
        self._graph = [set() for _ in range(v)]
    def add_edge(self, a, b):
        # метод добавления ребер
        self._graph[a].add(b)
        self._graph[b].add(a)
    def print_result(self):
        print(self._graph)
    def has_edge(self, a, b):
        # Метод проверки, есть ли между вершинами ребро
        print('Между вершиной ', a, ' и вершиной ', b, ' есть ребро:')
        if a in self._graph[b] and b in self._graph[a]:
            print('True')
        else:
            print('False')
    def remove_edge(self, a, b):
        # Метод удаления ребра
        if a in self._graph[b] and b in self._graph[a]:
            self._graph[a].discard(b)
            self._graph[b].discard(a)
        else:
            print('False')

g = Graph(10)
# Добавляем в граф ребра
g.add_edge(2, 8)
g.add_edge(8, 4)
g.add_edge(3, 8)
g.add_edge(8, 9)
g.add_edge(3, 6)
g.add_edge(6, 7)
g.add_edge(7, 9)
g.add_edge(5, 0)

g.print_result()

# Если повторно добавить то же самое ребро, ничего не поменяется
g.add_edge(5, 0)
g.print_result()

# Проверяем, есть ли ребра между вершинами
g.has_edge(1, 2)
g.has_edge(2, 8)

# Пытаемся удалить несуществующее ребро
g.remove_edge(1, 5)

# Удаляем существующее ребро
g.remove_edge(2, 8)

# Проверяем, что ребро действительно удалено
g.has_edge(2, 8)

